import Fastify, {FastifyInstance} from "fastify"
import {registerRoutes} from "@routing/routes";

(async function() {
    console.log("Initializing Server...")
    const host: string = process.env["SERVER_HOST"] ?? "localhost"
    const portVar: number = parseInt(process.env["SERVER_PORT"] ?? "");
    const port: number = (isNaN(portVar)) ? 80 : portVar;

    const server: FastifyInstance = Fastify({
        logger: true
    })

    console.log("Registering Routes...")
    registerRoutes(server)

    try {
        await server.listen({
            host,
            port
        })
        console.log(`HTTP Server listening on ${host}:${port}.`)
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
})()
