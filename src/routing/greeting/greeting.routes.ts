import {FastifyInstance} from "fastify";
import * as Get from "@routing/greeting/get/greeting-get.route"
import * as Post from "@routing/greeting/post/greeting-post.route"

export function registerGreetingRoutes(server: FastifyInstance, url: string): void {
    server.get(url, Get.opts, Get.handler)
    server.post(url, Post.opts, Post.handler)
}