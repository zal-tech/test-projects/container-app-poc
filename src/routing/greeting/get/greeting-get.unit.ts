import "mocha";
import {expect} from "chai";
import Fastify, {FastifyInstance, LightMyRequestResponse} from "fastify"
import * as Get from "@routing/greeting/get/greeting-get.route"

describe("Greeting-Get-Unit-Tests", function () {
    let app: FastifyInstance;

    before(function() {
        app = Fastify()
        app.get("/greeting", Get.opts, Get.handler)
    })

    describe("handler", function () {
        it("should return status code 200 and a static message", async function () {
            // Call API
            const response: LightMyRequestResponse = await app.inject({
                method: "GET",
                url: "/greeting"
            })

            // Assert Expectations
            expect(response.statusCode).to.equal(200)
            const respJson: Record<string, unknown> = response.json()
            expect(respJson['message']).to.equal("Hello")
        });
    });
});
