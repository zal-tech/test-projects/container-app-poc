import {FastifyReply, FastifyRequest, RouteHandlerMethod, RouteShorthandOptions} from "fastify";


export const handler: RouteHandlerMethod = async (request: FastifyRequest, reply: FastifyReply) => {
    return reply
        .code(200)
        .send({message: "Hello"})
}

export const opts: RouteShorthandOptions = {}
