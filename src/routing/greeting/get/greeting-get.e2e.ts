import "mocha";
import {expect} from "chai";
import {testApiAgent} from "@test/api-utils";
import {AxiosResponse} from "axios";

describe("Greeting-Get-E2E-Tests", function () {
    describe("handler", function () {
        it("should return status code 200 and a static message", async function () {
            // Call API
            const response: AxiosResponse = await testApiAgent.get("/greeting");

            // Assert Expectations
            expect(response.status).to.equal(200)
            expect(response.data).to.exist;
            const respJson: Record<string, unknown> = response.data
            expect(respJson['message']).to.equal("Hello")
        });
    });
});
