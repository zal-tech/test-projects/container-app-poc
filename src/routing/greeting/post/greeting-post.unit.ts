import "mocha";
import {expect} from "chai";
import Fastify, {FastifyInstance, LightMyRequestResponse} from "fastify"
import * as Post from "@routing/greeting/post/greeting-post.route"

describe("Greeting-Post-Unit-Tests", function () {
    let app: FastifyInstance;

    before(function() {
        app = Fastify()
        app.post("/greeting", Post.opts, Post.handler)
    })

    describe("handler", function () {
        it("should return status code 200 and a static message if no name is provided", async function () {
            // Call API
            const response: LightMyRequestResponse = await app.inject({
                method: "POST",
                url: "/greeting",
                body: {}
            })

            // Assert Expectations
            expect(response.statusCode).to.equal(200)
            const respJson: Record<string, unknown> = response.json()
            expect(respJson['message']).to.equal("Hello user")
        });

        it("should return status code 200 and a dynamic message if a name is provided", async function () {
            // Testing Parameters
            const name = "Dev"

            // Call API
            const response: LightMyRequestResponse = await app.inject({
                method: "POST",
                url: "/greeting",
                body: {
                    name
                }
            })

            // Assert Expectations
            expect(response.statusCode).to.equal(200)
            const respJson: Record<string, unknown> = response.json()
            expect(respJson['message']).to.equal(`Hello ${name}`)
        });
    });
});
