import "mocha";
import {expect} from "chai";
import {AxiosResponse} from "axios";
import {testApiAgent} from "@test/api-utils";

describe("Greeting-Post-E2E-Tests", function () {
    describe("handler", function () {
        it("should return status code 200 and a static message if no name is provided", async function () {
            // Call API

            // Call API
            const response: AxiosResponse = await testApiAgent.post("/greeting", {});

            // Assert Expectations
            expect(response.status).to.equal(200)
            expect(response.data).to.exist;
            const respJson: Record<string, unknown> = response.data
            expect(respJson['message']).to.equal("Hello user")
        });

        it("should return status code 200 and a dynamic message if a name is provided", async function () {
            // Testing Parameters
            const name = "Dev"

            // Call API
            const response: AxiosResponse = await testApiAgent.post("/greeting", {
                name
            });

            // Assert Expectations
            expect(response.status).to.equal(200)
            expect(response.data).to.exist;
            const respJson: Record<string, unknown> = response.data
            expect(respJson['message']).to.equal(`Hello ${name}`)
        });
    });
});
