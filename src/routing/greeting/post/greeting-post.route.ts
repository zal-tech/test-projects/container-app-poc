import {FastifyReply, FastifyRequest, RouteHandlerMethod, RouteShorthandOptions} from "fastify";

export const handler: RouteHandlerMethod = async(request: FastifyRequest, reply: FastifyReply) => {
    const body: Record<string, unknown> = request.body as Record<string, unknown>
    const name: string | undefined = body['name'] as string ?? "user";

    return reply
        .code(200)
        .send({message: `Hello ${name}`})
}

export const opts: RouteShorthandOptions = {
    schema: {
        body: {
            type: "object",
            properties: {
                name: {
                    type: "string"
                }
            }
        }
    }
}
