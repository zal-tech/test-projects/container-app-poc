import {FastifyInstance} from "fastify";
import {registerGreetingRoutes} from "@routing/greeting/greeting.routes";


export function registerRoutes(server: FastifyInstance): void {
    registerGreetingRoutes(server, "/greeting")
    // Add additional top-level resource routes here
}