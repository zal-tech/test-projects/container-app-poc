"use strict";
const path = require("path");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin").TsconfigPathsPlugin;

// Webpack Config Function
module.exports = async function () {
	return {
		mode: "none",
		entry: "./src/server.ts",
		output: {
			path: path.join(__dirname, "build"),
			filename: "server.js",
			libraryTarget: "commonjs2",
		},
		module: {
			rules: [
				{
					test: /\.ts$/,
					use: ["ts-loader"],
					include: path.resolve(__dirname, "src"),
				},
			],
		},
		resolve: {
			extensions: [".ts", ".js"],
			plugins: [new TsConfigPathsPlugin({})],
		},
		optimization: {
			concatenateModules: true,
			emitOnErrors: true,
			minimize: true,
			moduleIds: "named",
			providedExports: true,
			removeAvailableModules: false,
			sideEffects: true,
			usedExports: true,
		},
		target: "node",
		stats: {
			all: false,
			cached: true,
			entrypoints: true,
			env: true,
			errors: true,
			errorDetails: true,
			moduleTrace: true,
			performance: true,
			timings: true,
			warnings: true,
		},
		devtool: "inline-source-map",
	};
};
