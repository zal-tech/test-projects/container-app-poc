"use strict";

module.exports = {
    reporter: [
        "mocha-junit-reporter",
        "spec"
    ],
    "reporter-option": [
        "mochaFile=./reports/mocha-results.xml"
    ],
    require: ["ts-node/register", "tsconfig-paths/register"],
    timeout: 2500,
    ui: "bdd",
};
