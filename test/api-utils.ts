import axios, { AxiosInstance } from "axios";

export const testApiUrl: string = ((): string => {
	const host: string = process.env["SERVER_HOST"] ?? "http://localhost"

	const parsePort: number = parseInt(process.env["SERVER_PORT"] ?? "");
	const port: number = (isNaN(parsePort)) ? 80 : parsePort;

	return `${host}:${port}/`;
})();

export const testApiAgent: AxiosInstance = axios.create({
	baseURL: testApiUrl,
	headers: {
		"content-type": "application/json",
	},
});
